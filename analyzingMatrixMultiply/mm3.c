#include <stdio.h>

#define XSIZE 160
#define YSIZE 160

int main(void) {

  int x[XSIZE][YSIZE];
  int y[XSIZE][YSIZE];
  int z[XSIZE][YSIZE];

  int i, j, k;
  

  for (i=0; i<XSIZE; i++){
    for(j=0; j<XSIZE; j++){
      x[i][j] = 0;
    }
  }

  /* Initialize y matrix */
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
      y[i][j] = i + j;
    }
  }

 

  /* Initialize z matrix */
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
	z[i][j] = i + j;
    }
  }

 
  /* Do matrix multiply */
  /* INSERT CODE HERE. */
  for (i=0; i < XSIZE; i+=2){
    for (j=0; j < XSIZE; j+=2){
      int a = 0;
      int b = 0;
      int c = 0;
      int d = 0;
      for(k=0; k < XSIZE; k+=4){
        a = a + y[i][k]*z[k][j] + y[i][k+1]*z[k+1][j] + y[i][k+2]*z[k+2][j] + y[i][k+3]*z[k+3][j];
        b = b + y[i+1][k]*z[k][j] + y[i+1][k+1]*z[k+1][j] + y[i+1][k+2]*z[k+2][j] + y[i+1][k+3]*z[k+3][j];
        c = c + y[i][k]*z[k][j+1] + y[i][k+1]*z[k+1][j+1] + y[i][k+2]*z[k+2][j+1] + y[i][k+3]*z[k+3][j+1];
        d = d + y[i+1][k]*z[k][j+1] + y[i+1][k+1]*z[k+1][j+1] + y[i+1][k+2]*z[k+2][j+1] + y[i+1][k+3]*z[k+3][j+1];
      }
      x[i][j] = a;
      x[i+1][j] = b;
      x[i][j+1] = c;
      x[i+1][j+1] = d;
    }
  }
}
