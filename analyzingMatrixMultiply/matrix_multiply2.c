#include <stdio.h>
#include <math.h>
#include <time.h>

#define XSIZE 700
#define YSIZE 700

void printMatrix(int arr[XSIZE][YSIZE]){
  int i, j;
  for (i=0; i < XSIZE; i++){
    for (j=0; j<YSIZE; j++){
      printf("%d ", arr[i][j]);
    }
    printf("\n");
  }
}
int main(void) {

  int x[XSIZE][YSIZE];
  int y[XSIZE][YSIZE];
  int z[XSIZE][YSIZE];

  int i, j, k;
  
  int r;

  clock_t begin, end;

  double time_spent;

  // initialize x matrix
  for (i=0; i<XSIZE; i++){
    for(j=0; j<XSIZE; j++){
      x[i][j] = 0;
    }
  }

  /* Initialize y matrix */
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
      y[i][j] = i + j;
    }
  }

 

  /* Initialize z matrix */
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
	z[i][j] = i + j;
    }
  }

  begin = clock();
 
  /* Do matrix multiply */
  /* INSERT CODE HERE. */
  // x11 = (y11 x z11) + (y12 x z21) + (y13 x z31) + (y14 x z41)
  for (i=0; i < XSIZE; i+=2){
    for (j=0; j < XSIZE; j+=2){
      for(k=0; k < XSIZE; k+=4){
        x[i][j] = x[i][j] + y[i][k]*z[k][j] + y[i][k+1]*z[k+1][j] + y[i][k+2]*z[k+2][j] + y[i][k+3]*z[k+3][j];
        x[i+1][j] = x[i+1][j] + y[i+1][k]*z[k][j] + y[i+1][k+1]*z[k+1][j] + y[i+1][k+2]*z[k+2][j] + y[i+1][k+3]*z[k+3][j];
        x[i][j+1] = x[i][j+1] + y[i][k]*z[k][j+1] + y[i][k+1]*z[k+1][j+1] + y[i][k+2]*z[k+2][j+1] + y[i][k+3]*z[k+3][j+1];
        x[i+1][j+1] = x[i+1][j+1] + y[i+1][k]*z[k][j+1] + y[i+1][k+1]*z[k+1][j+1] + y[i+1][k+2]*z[k+2][j+1] + y[i+1][k+3]*z[k+3][j+1];
      }
    }
  }


  end = clock();
  
  //printMatrix(x);

  time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
  
  printf("Time Spent on MM is:  %lf\n", time_spent);
}
