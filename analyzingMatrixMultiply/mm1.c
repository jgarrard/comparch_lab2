#include <stdio.h>

#define XSIZE 160
#define YSIZE 160

int main(void) {

  int x[XSIZE][YSIZE];
  int y[XSIZE][YSIZE];
  int z[XSIZE][YSIZE];

  int i, j, k;
  
  for (i=0; i<XSIZE; i++){
    for(j=0; j<XSIZE; j++){
      x[i][j] = 0;
    }
  }

  /* Initialize y matrix */
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
      y[i][j] = i + j;
    }
  }

  /* Initialize z matrix */
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
	z[i][j] = i + j;
    }
  }

 
  /* Do matrix multiply */
  /* INSERT CODE HERE. */
  for (i=0; i < XSIZE; i++){
    for (j=0; j < XSIZE; j++){
      for(k=0; k < XSIZE; k++){
        x[i][j] = x[i][j] + y[i][k]*z[k][j];
      }
    }
  }
}
