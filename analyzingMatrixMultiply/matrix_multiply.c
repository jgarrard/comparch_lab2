#include <stdio.h>
#include <math.h>
#include <time.h>

#define XSIZE 160
#define YSIZE 160

void printMatrix(int arr[XSIZE][YSIZE]){
  int i, j;
  for (i=0; i < XSIZE; i++){
    for (j=0; j<YSIZE; j++){
      printf("%d ", arr[i][j]);
    }
    printf("\n");
    printf("\n");
  }
}
int main(void) {

  int x[XSIZE][YSIZE];
  int y[XSIZE][YSIZE];
  int z[XSIZE][YSIZE];

  int i, j, k;
  
  int r;

  clock_t begin, end;

  double time_spent;

  // initialize x matrix
  for (i=0; i<XSIZE; i++){
    for(j=0; j<XSIZE; j++){
      x[i][j] = 0;
    }
  }

  /* Initialize y matrix */
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
      y[i][j] = i + j;
    }
  }

 

  /* Initialize z matrix */
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
	z[i][j] = i + j;
    }
  }

  begin = clock();
 
  /* Do matrix multiply */
  /* INSERT CODE HERE. */
  // x11 = (y11 x z11) + (y12 x z21) + (y13 x z31) + (y14 x z41)
  for (i=0; i < XSIZE; i++){
    for (j=0; j < XSIZE; j++){
      for(k=0; k < XSIZE; k++){
        x[i][j] = x[i][j] + y[i][k]*z[k][j];
      }
    }
  }


  end = clock();
  
  //printMatrix(x);

  //time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
  
  //printf("Time Spent on MM is:  %lf\n", time_spent);
}
