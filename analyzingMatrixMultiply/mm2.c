#include <stdio.h>

#define XSIZE 160
#define YSIZE 160

int main(void) {

  int x[XSIZE][YSIZE];
  int y[XSIZE][YSIZE];
  int z[XSIZE][YSIZE];

  int i, j, k;
  
  for (i=0; i<XSIZE; i++){
    for(j=0; j<XSIZE; j++){
      x[i][j] = 0;
    }
  }

  /* Initialize y matrix */
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
      y[i][j] = i + j;
    }
  }

 

  /* Initialize z matrix */
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
	z[i][j] = i + j;
    }
  }

 
  /* Do matrix multiply */
  /* INSERT CODE HERE. */
  for (i=0; i < XSIZE; i+=2){
    for (j=0; j < XSIZE; j+=2){
      for(k=0; k < XSIZE; k+=4){
        x[i][j] = x[i][j] + y[i][k]*z[k][j] + y[i][k+1]*z[k+1][j] + y[i][k+2]*z[k+2][j] + y[i][k+3]*z[k+3][j];
        x[i+1][j] = x[i+1][j] + y[i+1][k]*z[k][j] + y[i+1][k+1]*z[k+1][j] + y[i+1][k+2]*z[k+2][j] + y[i+1][k+3]*z[k+3][j];
        x[i][j+1] = x[i][j+1] + y[i][k]*z[k][j+1] + y[i][k+1]*z[k+1][j+1] + y[i][k+2]*z[k+2][j+1] + y[i][k+3]*z[k+3][j+1];
        x[i+1][j+1] = x[i+1][j+1] + y[i+1][k]*z[k][j+1] + y[i+1][k+1]*z[k+1][j+1] + y[i+1][k+2]*z[k+2][j+1] + y[i+1][k+3]*z[k+3][j+1];
      }
    }
  }
}
