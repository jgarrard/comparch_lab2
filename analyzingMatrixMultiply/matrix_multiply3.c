#include <stdio.h>
#include <math.h>
#include <time.h>

#define XSIZE 700
#define YSIZE 700

void printMatrix(int arr[XSIZE][YSIZE]){
  int i, j;
  for (i=0; i < XSIZE; i++){
    for (j=0; j<YSIZE; j++){
      printf("%d ", arr[i][j]);
    }
    printf("\n");
  }
}
int main(void) {

  int x[XSIZE][YSIZE];
  int y[XSIZE][YSIZE];
  int z[XSIZE][YSIZE];

  int i, j, k;
  
  int r;

  clock_t begin, end;

  double time_spent;

  // initialize x matrix
  for (i=0; i<XSIZE; i++){
    for(j=0; j<XSIZE; j++){
      x[i][j] = 0;
    }
  }

  /* Initialize y matrix */
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
      y[i][j] = i + j;
    }
  }

 

  /* Initialize z matrix */
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
	z[i][j] = i + j;
    }
  }

  begin = clock();
 
  /* Do matrix multiply */
  /* INSERT CODE HERE. */
  // x11 = (y11 x z11) + (y12 x z21) + (y13 x z31) + (y14 x z41)
  for (i=0; i < XSIZE; i+=2){
    for (j=0; j < XSIZE; j+=2){
      int a = 0;
      int b = 0;
      int c = 0;
      int d = 0;
      for(k=0; k < XSIZE; k+=4){
        a = a + y[i][k]*z[k][j] + y[i][k+1]*z[k+1][j] + y[i][k+2]*z[k+2][j] + y[i][k+3]*z[k+3][j];
        b = b + y[i+1][k]*z[k][j] + y[i+1][k+1]*z[k+1][j] + y[i+1][k+2]*z[k+2][j] + y[i+1][k+3]*z[k+3][j];
        c = c + y[i][k]*z[k][j+1] + y[i][k+1]*z[k+1][j+1] + y[i][k+2]*z[k+2][j+1] + y[i][k+3]*z[k+3][j+1];
        d = d + y[i+1][k]*z[k][j+1] + y[i+1][k+1]*z[k+1][j+1] + y[i+1][k+2]*z[k+2][j+1] + y[i+1][k+3]*z[k+3][j+1];
      }
      x[i][j] = a;
      x[i+1][j] = b;
      x[i][j+1] = c;
      x[i+1][j+1] = d;
    }
  }
  


  end = clock();

  //printMatrix(x);
  time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
  
  printf("Time Spent on MM is:  %lf\n", time_spent);
}
